# Task Management Application Challenge

## Objective:

Create a basic task management web application using Ruby on Rails.

### Features to Include:

- **User Authentication:** Implement user sign up, login, and logout functionality.
- **Task CRUD Operations:** Users should be able to create, read, update, and delete tasks.
- **Task Status:** Each task should have a status (e.g., pending, completed).
- **Task Comments:** Users should be able to add comments to tasks for discussion or clarification.

### Bonus Features (Optional):

- **Task Filtering and Sorting:** Users should be able to filter and sort tasks based on categories, status, due date, etc.

### Instructions:

- Fork the project
- Use Ruby on Rails framework to develop the application.
- Utilize basic MVC (Model-View-Controller) architecture.
- Focus on functionality over complexity.
- Keep the design simple and minimalistic.

### Evaluation Criteria:

- **Functionality:** Does the application fulfill the specified features and requirements?
- **Code Quality:** Is the code well-organized and readable?
- **User Experience:** Is the interface simple and easy to use?

  
  
## Initial configuration Rails 7 with docker boilerplate

### Features

* Rails 7
* Ruby 3
* Dockerfile and Docker Compose configuration
* PostgreSQL database
* Redis

### Requirements

Please ensure you are using Docker Compose V2. This project relies on the `docker compose` command, not the previous `docker-compose` standalone program.

https://docs.docker.com/compose/#compose-v2-and-the-new-docker-compose-command

Check your docker compose version with:
```
% docker compose version
Docker Compose version v2.10.2
```

### Initial setup
```
cp .env.example .env
docker compose build
docker compose run --rm web bin/rails db:setup
```

### Running the Rails app
```
docker compose up
```

### Running the Rails console
When the app is already running with `docker-compose` up, attach to the container:
```
docker compose exec web bin/rails (c / db:migrate / etc. all comands for rails)
```

When no container running yet, start up a new one:
```
docker compose run --rm web bin/rails (c / db:migrate / etc. all comands for rails)
```

### Running tests
```
docker compose run --rm web bin/rspec
```

### Updating gems
```
docker compose run --rm web bundle update
docker compose up --build
```